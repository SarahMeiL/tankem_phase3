# -*- coding: utf-8 -*-

import os, cx_Oracle
from data import *
from Tkinter import Tk
from tkFileDialog import asksaveasfilename
import tkMessageBox 

def select_save_csv():  
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop') 
    return asksaveasfilename(
        filetypes=[("CSV files", "*.csv"), ("All files", "*")], defaultextension="*.csv", initialfile="BalanceTankem.csv", initialdir=desktop)

def main():
    
    Tk().withdraw()
    try:
        oracle_dao = Balance_Oracle_DAO()
        csv_dao = Balance_CSV_DAO()
        dto = Balance_DTO()       
       
        fichier = select_save_csv()

        oracle_dao.getGameAttributes(dto)
        csv_dao.updateGameAttributes(dto, fichier)
        
        os.startfile(fichier)
    except IOError:
        tkMessageBox.showinfo("ERREUR", "Impossible de lire le fichier " + fichier)	
    except cx_Oracle.DatabaseError:
        tkMessageBox.showinfo("ERREUR", "Impossible de se connecter à la base de donnée")	      

if __name__ == '__main__':
    main()