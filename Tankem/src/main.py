## -*- coding: utf-8 -*-
#Ajout des chemins vers les librarires
from util import inclureCheminCegep
import sys

#Importe la configuration de notre jeu
from panda3d.core import loadPrcFile
loadPrcFile("config/ConfigTankem.prc")

#Module de Panda3DappendObject
from direct.showbase.ShowBase import ShowBase

#Modules internes
from gameLogic import GameLogic
from interface import InterfaceMenuPrincipal, InterfaceMapSelector, InterfaceLogin


#Modules necessaires au fonctionnement de la balance et des maps
from data import *

from random import randint

from direct.showbase.Transitions import Transitions

 
class Tankem(ShowBase):
    def __init__(self, balanceDTO , mapDTO,joueurDTO,dto_statisque):
        ShowBase.__init__(self)
        self.demarrer(balanceDTO, mapDTO,joueurDTO,dto_statisque)

    def demarrer(self, balanceDTO , mapDTO,joueurDTO,dto_statisque):        
        self.gameLogic = GameLogic(self, balanceDTO, mapDTO,joueurDTO,dto_statisque)
        #Commenter/décommenter la ligne de votre choix pour démarrer le jeu
        #Démarre dans le menu
        #self.menuPrincipal = InterfaceMenuPrincipal()
        #self.login = InterfaceLogin(joueurDTO) 
       # self.interface = InterfaceMapSelector(mapDTO)
        # PHASE 2 ##################################################################################################
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)


        self.accept("loadSelector",lambda: InterfaceMapSelector(mapDTO,self.transition))

        self.accept("loadLogin",lambda: InterfaceLogin(joueurDTO,mapDTO.infoNiveau['nom'],self.transition))
        ############################################################################################################

        self.accept("loadMenu", lambda: InterfaceMenuPrincipal())  

        #Démarre directement dans le jeu
        messenger.send("loadSelector")

    

#Main de l'application.. assez simple!
dto = Balance_DTO()
balance_dto = Balance_DTO()
map_dto = Map_DTO()

dto_joueur = DTO_Joueur()
dto_statisque = DTO_Statistique()

try:     
    dao = Balance_Oracle_DAO()
    dao.getGameAttributes(dto)   

except:    
    dto.textData["message_lobby_content"] = "Problème de connexion. Configuration par défaut utilisée."
    dto.numData["message_lobby_duration"] = 6

app = Tankem(dto,map_dto,dto_joueur,dto_statisque)
app.run()