## -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
#from direct.directbase import DirectStart
from panda3d.core import *
from panda3d.bullet import BulletWorld # REMOVE IF NOT USED

from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys
from map import Map
from data import *
class InterfaceMapSelector(ShowBase):
    def __init__(self,mapDTO,transition):
        self.accept("arrow_up",lambda: self.moveCursor(-1))
        self.accept("arrow_down",lambda: self.moveCursor(1))
        #self.accept("enter",self.launchGame)

        self.transition = transition

        #####
        # Recuperer données initiales
        ############################################################################################        
        self.dto = mapDTO  
        self.dao = None
        try:
            self.dao = DAO_Map_Oracle() 
            self.dao.getNiveauxActif(self.dto) 
        except:    
            self.textErreur = OnscreenText(text = "Erreur de connexion à la BD !",
                                      pos = (-1.65,0.8), 
                                      scale = 0.15,
                                      fg=(0.6,0.1,0.1,1),
                                      align=TextNode.ALeft)      

        #####
        # Liste
        ############################################################################################ 
        numItemsVisibles= 7
        itemHeight = 0.16
         
        self.myScrolledList = DirectScrolledList(
            #Btn DESCENDRE
            incButton_pos= (0.62, 0, -0.69),
            incButton_text = "DESCENDRE",            
            incButton_text_scale = 0.075,
            incButton_borderWidth = (0.005, 0.005),
            incButton_pad = (0.39,0.01),
            incButton_frameColor=(0.5,0.5,0.5,0.95),
            incButton_text_fg=(0.1,0.1,0.1,0.95),

            #Btn MONTER
            decButton_pos= (0.62, 0, 0.65),
            decButton_text = "MONTER",            
            decButton_text_scale = 0.075,
            decButton_borderWidth = (0.005, 0.005),
            decButton_pad = (0.445,0.01),            
            decButton_frameColor=(0.5,0.5,0.5,0.95),
            decButton_text_fg=(0.1,0.1,0.1,0.95),           
         
            #Frame
            frameSize = (0, 1.25, -0.75, 0.95),
            frameColor = (0.1,0.1,0.1,0.5),
            pos = (0.4, 0, 0),
            
            #Item Frame
            numItemsVisible = numItemsVisibles,
            forceHeight = itemHeight,
            itemFrame_frameSize = (-0.6, 0.6, -1, 0.2),
            itemFrame_pos = (0.625, 0, 0.4),
            itemFrame_frameColor = (0.1,0.1,0.1,0.8),
            )
         
        #####
        # Insertions
        ############################################################################################
        #Btn Random
        self.listeBTNS = []
        btnRandom=DirectButton(text = ("Random", "Random", "Random", "disabled"),
                          text_scale=0.1, 
                          borderWidth = (0.01, 0.01),
                          text_pos=(0,0.06),
                          frameColor=(0.2,0.2,0.2,0.95),
                          text_fg=(1,1,1,0.95),
                          relief=2, 
                          frameSize = (-0.56,0.56,0.02,0.16), 
                          command=lambda:self.highlight("Random"))
        self.listeBTNS.append(btnRandom)
        self.myScrolledList.addItem(btnRandom)
        
        #Maps du DTO  
        for mapName in self.dto.nomActifData:           
            btn=DirectButton(text = (mapName, mapName, mapName, "disabled"),
                        text_scale=0.1, 
                        borderWidth = (0.01, 0.01),
                        frameColor=(0.2,0.2,0.2,0.95),
                        text_fg=(1,1,1,0.95),
                        text_pos=(0,0.06),
                        extraArgs=[mapName],
                        relief=2, 
                        frameSize = (-0.56,0.56,0.02,0.16), 
                        command=self.highlight)

            self.listeBTNS.append(btn)
            self.myScrolledList.addItem(btn)

        #Random est selected par defaut
        self.selectedMap = "Random"
        self.currIndex = 0
        self.highlight("Random")

        #####
        # Reste du UI
        ############################################################################################
                
        #Image d'arrière plan
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menuMapSelector.jpg")       

        #Titre de la page
        self.textTitre = OnscreenText(text = "Selecteur de map",
                                      pos = (0.55,0.8), 
                                      scale = 0.12,
                                      fg=(0.1,0.1,0.1,1),
                                      align=TextNode.ALeft)        
        
        #Bouton Start 
        couleurBack = (0.243,0.325,0.121,1)   
        btnScale = (0.10,0.10)
        text_scale = 0.8
        borderW = (0.04, 0.04)    
        self.b1 = DirectButton(text = ("Tank'em !", "Jouer !", "Start !", "disabled"),
            text_scale=btnScale,
            borderWidth = borderW,
            text_bg=couleurBack,
            frameColor=couleurBack,
            relief=2,
            command = self.chargerInterfaceLogin,          
            pos = (1.4,1,-0.9)
        )

        #Initialisation de l'effet de transition
        # curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        # self.transition = Transitions(loader)
        # self.transition.setFadeColor(0, 0, 0)
        # self.transition.setFadeModel(curtain)

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")
        numItemsVisible = 8
        itemHeight = 0.3

    def highlight(self,name):        
        self.selectedMap = name
        for btn in self.listeBTNS:                        
            #Si name dans dans btn actuel, changer couleur et index            
            if name in btn["text"]:               
                btn.configure(frameColor=(1,0.2,0.2,0.95))
                self.currIndex = self.listeBTNS.index(btn)                                
            else:                
                btn.configure(frameColor=(0.2,0.2,0.2,0.95))
   
    def cacher(self):               
        #On cache les menus
        self.background.hide()
        self.b1.hide()    
        self.textTitre.hide()
        self.myScrolledList.hide()
        #Cacher erreur si affichée
        try:
            self.textErreur.hide()
        except:
            pass

    def moveCursor(self,delta):        
        #Tant que l'index est valide 
        #Note : Index negatif va aller au dernier index et assigner le bon index dans la methode highlight
        try:
            #Recuperer parametre text DirectGUI du prochain bouton dans la liste
            nextName = self.listeBTNS[self.currIndex + delta]["text"][0]
            self.highlight(nextName)
            self.myScrolledList.scrollTo(self.currIndex,centered=0)
            
        except:            
                #Sinon, IndexOutOfBound, donc loop around a l'index 0                
                nextName = self.listeBTNS[0]["text"][0]                
                self.myScrolledList.scrollTo(0,centered=0)
                self.highlight(nextName)

    def retrieveSelectedMap(self):
        #If not random, retrive selected map
        if self.selectedMap != "Random" and self.dao:
            self.dao.getNiveauParNom(self.selectedMap,self.dto)
        else:
            self.dto.infoNiveau['nom'] = 'Random'
    
    def launchGame(self):
        #Launch Sequence    
        Sequence(Func(lambda : self.transition.irisOut(0.05)),
                SoundInterval(self.sound),
                Func(self.cacher),
                Func(self.retrieveSelectedMap),
                Func(lambda : messenger.send("DemarrerPartie")),                
                Func(lambda : self.transition.irisIn(0.05))
        ).start()
    
    def chargerInterfaceLogin(self):
        #On démarre!
        Sequence(Func(lambda : self.transition.irisOut(0.2)),
                SoundInterval(self.sound),
                Func(self.cacher),
                Func(self.retrieveSelectedMap),
                # Func(lambda : messenger.send("DemarrerLogin",[self.dto.infoNiveau['nom']])),
                Func(lambda : messenger.send("loadLogin")),
                Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                Func(lambda : self.transition.irisIn(0.2))
        ).start()  