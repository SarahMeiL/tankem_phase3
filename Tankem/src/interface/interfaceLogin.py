## -*- coding: utf-8 -*-
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys
from util.outilCalcul import AnalyseDTOJoueur
import random
from data.DAO_Joueur import DAO_Joueur
from data.Joueur import Joueur
from random import randint

class InterfaceLogin(ShowBase):
    def __init__(self, joueurDTO, nomMap,transition):

        base.buttonThrowers[0].node().setButtonDownEvent('button')
        self.accept( 'button', self.setText)

        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menuLog.jpg")
        self.dao = None
        self.dto = joueurDTO

        self.tank1 = None
        self.tank2 = None
        self.seq1 = None
        self.seq2 = None

        self.pass1 = ""
        self.pass2 = ""

        self.transition = transition

        self.focusPass1 = False
        self.focusPass2 = False


        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        base.disableMouse()

        #Boutons
        btnScale = (0.1,0.1)
        text_scale = 0.10
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 0
        hauteur = -0.8
        self.frMain = DirectFrame(
            frameSize = (-1.5, 1.5, -1, 1),
            frameColor = (0, 0, 0, 0.70)
        )
        self.b1 = DirectButton(parent = self.frMain, text = ("Combattre", "!", "!", "disabled"),
                        text_scale=btnScale,
                        borderWidth = borderW,
                        text_bg=couleurBack,
                        frameColor=couleurBack,
                        relief=2,
                        command=self.launchGame,
                        pos = (separation,0,hauteur))

        #Initialisation de l'effet de transition
        # curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        # self.transition = Transitions(loader)
        # self.transition.setFadeColor(0, 0, 0)
        # self.transition.setFadeModel(curtain)

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")

        self.bConnexion1 = DirectButton(parent = self.frMain, text = ("Connexion", "!", "!", "disabled"),
                                  text_scale = (0.07,0.07),
                                  borderWidth = borderW,
                                  text_bg = couleurBack,
                                  frameColor = couleurBack,
                                  relief = 2,
                                  command = self.connecterJoueur1,
                                  pos = (-1.1,0,0.25))

        self.bConnexion2 = DirectButton(parent = self.frMain, text = ("Connexion", "!", "!", "disabled"),
                                  text_scale = (0.07,0.07),
                                  borderWidth = borderW,
                                  text_bg = couleurBack,
                                  frameColor = couleurBack,
                                  relief = 2,
                                  command = self.connecterJoueur2,
                                  pos = (1.05,0,0.25))
    
        self.textObject = OnscreenText(
            parent = self.frMain,
            text = 'Nom: ', 
            pos = (-1.22, 0.85),
            scale = 0.05, 
            fg=(1,1,1,1), 
            align=TextNode.ACenter,
            mayChange=1
        )

        self.textObject2 = OnscreenText(
            parent = self.frMain,
            text = 'Nom: ', 
            pos = (0.65, 0.85),
            scale = 0.05, 
            fg=(1,1,1,1),  
            align=TextNode.ACenter,
            mayChange=1
        )

        self.textObject3 = OnscreenText(
            parent = self.frMain,
            text = 'Mot de passe: ', 
            pos = (-1.12, 0.6),
            scale = 0.05, 
            fg=(1,1,1,1),  
            align=TextNode.ACenter,
            mayChange=1
        )

        self.textObject4 = OnscreenText(
            parent = self.frMain,
            text = 'Mot de passe: ', 
            pos = (0.75, 0.6),
            scale = 0.05, 
            fg=(1,1,1,1), 
            align=TextNode.ACenter,
            mayChange=1
        )

        self.textObject5 = OnscreenText(
            parent = self.frMain,
            text = "",
            pos = (0, 0.1),
            scale = 0.05, 
            fg=(1,1,1,1),  
            align=TextNode.ACenter,
            mayChange=1
        )

        self.textObject6 = OnscreenText(
            parent = self.frMain,
            text = 'VS',
            pos = (0, 0.03),
            scale = 0.05, 
           fg=(1,1,1,1),  
            align=TextNode.ACenter,
            mayChange=1
        )

        self.textObject7 = OnscreenText(
            parent = self.frMain,
            text = "",
            pos = (0, -0.03),
            scale = 0.05, 
            fg=(1,1,1,1), 
            align=TextNode.ACenter,
            mayChange=1
        )
        
        self.textObject8 = OnscreenText(
            parent = self.frMain,
            text = "",
            pos = (0, -0.3),
            scale = 0.05, 
            fg=(1,1,1,1),   
            align=TextNode.ACenter,
            mayChange=1
        )

        self.textObject9 = OnscreenText(
            parent = self.frMain,
            text = "Les deux joueurs vont s'affronter dans "+str(nomMap),
            pos = (0, -0.5),
            scale = 0.05, 
            fg=(1,1,1,1),   
            align=TextNode.ACenter,
            mayChange=1
        )

        self.entreeNom1 = DirectEntry(
            parent = self.frMain,
            pos = (-1.27, 0, 0.75),
            scale = 0.06,
            initialText="",
            numLines = 1,
            focus = 1
        )

        self.entreeNom2 = DirectEntry(
            parent = self.frMain,
            pos = (0.6, 0, 0.75),
            scale = 0.06,
            initialText="",
            numLines = 1,
            focus = 1
        )

        self.entreePass1 = DirectEntry(
            parent = self.frMain,
            text = "",
            pos = (-1.27, 0, 0.49),
            scale = 0.06,
            initialText="",
            focusInCommand=self.inFocus,
            focusInExtraArgs=["entreePass1"],
            focusOutCommand=self.outFocus,
            focusOutExtraArgs=["entreePass1"],
            numLines = 1,
            focus = 1
        )

        self.entreePass2 = DirectEntry(
            parent = self.frMain,
            text = "",
            pos = (0.6, 0, 0.49),
            scale = 0.06,
            initialText="",
            focusInCommand=self.inFocus,
            focusInExtraArgs=["entreePass2"],
            focusOutCommand=self.outFocus,
            focusOutExtraArgs=["entreePass2"],
            numLines = 1,
            focus = 1
        )

        self.entreeMess = DirectEntry(
            parent = self.frMain,
            text = "",
            pos = (-0.8, 0, 0.25),
            width = 22,
            scale = 0.07,
            numLines = 1,
        )



        try:
            self.dao = DAO_Joueur()
            self.entreeMess.set("Connectez-vous!")
            self.textObject6.hide()
            self.b1.hide()
            self.textObject9.hide()
        except:
            self.entreeMess.set("Erreur basse de donnée!")
            self.loadDefaultTank()
            self.textObject.hide()
            self.textObject2.hide()
            self.textObject3.hide()
            self.textObject4.hide()
            self.entreeNom1.hide()
            self.entreeNom2.hide()
            self.entreePass1.hide()
            self.entreePass2.hide()
            self.bConnexion1.hide()
            self.bConnexion2.hide()

    def inFocus(self,focus):
        if focus == "entreePass1":
            self.focusPass1 = True
        elif focus == "entreePass2":
            self.focusPass2 = True

    def outFocus(self,focus):
        if focus == "entreePass1":
            self.focusPass1 = False
        elif focus == "entreePass2":
            self.focusPass2 = False

    def setText(self,key):
        pass
        # if len(key) == 1 or (key == 'space' or key == 'backspace'):
        #     if self.focusPass1:
        #         if key == 'backspace':
        #             self.pass1 = self.pass1[1:]
        #         elif key == 'space':
        #             self.pass1 += ' '
        #         else:
        #             self.pass1 += key
        #         chaine = ""
        #         for i in range(len(self.pass1)):
        #             chaine += "*"

        #         self.entreePass1.set(chaine)
                
        #     if self.focusPass2:
        #         if key == 'backspace':
        #             self.pass2 = self.pass1[1:]
        #         elif key == 'space':
        #             self.pass2 += ' '
        #         else:
        #             self.pass2 += key
        #         chaine = ""
        #         for i in range(len(self.pass2)):
        #             chaine += "*"

        #         self.entreePass2.set(chaine)

    def clearText(self):
        self.entreeNom1.enterText('')
        self.entreeNom2.enterText('')
        self.entreePass1.enterText('')
        self.entreePass2.enterText('')
        self.entreeMess.enterText('')

    def chargeMenu(self):
        #On démarre!
        Sequence(Func(lambda : self.transition.irisOut(0.2)),
            SoundInterval(self.sound),
            Func(self.cacher),

            # PHASE 3 ##################################################################################################
            Func(lambda : messenger.send("loadMenu")),
            ############################################################################################################

            Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
            Func(lambda : self.transition.irisIn(0.2))
        ).start()

    def cacher(self):               
        #On cache les menus
        self.seq1.finish()
        self.seq2.finish()
        self.tank1.hide()
        self.tank2.hide()
        self.background.hide()
        self.b1.hide()    
        self.textObject.hide()
        self.textObject2.hide()
        self.textObject3.hide()
        self.textObject4.hide()
        self.textObject5.hide()
        self.textObject6.hide()
        self.textObject7.hide()
        self.textObject8.hide()
        self.textObject9.hide()
        self.entreeNom1.hide()
        self.entreeNom2.hide()
        self.entreePass1.hide()
        self.entreePass2.hide()
        self.entreeMess.hide()
        self.bConnexion1.hide()
        self.bConnexion2.hide()
        self.frMain.hide()

        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
        self.ignore("button")

        #Cacher erreur si affichée
        try:
            self.textErreur.hide()
        except:
            pass

    def charTournant(self,tank,tour):
        dureeSequence = 2.5
        angle = Point3(tour, 0, 0)
        angleDepart = tank.getHpr()
        angleFin = angleDepart + angle
        intervalTour = tank.hprInterval(dureeSequence, angleDepart, startHpr = angleFin)
        #intervalSautBas = self.tank1.hprInterval(dureeSequence/2.0, posSautBas, startHpr = posSautHaut, blendType = 'easeOut')
        seq = Sequence(intervalTour)
        seq.loop()

        return seq
    

    def creerChar(self,joueur,pos,newPos):
        tank = loader.loadModel("../asset/Tank/tank")
        tank.setColorScale(joueur.couleur[0], joueur.couleur[1], joueur.couleur[2], 1)
        tank.setPos(pos[0], pos[1], pos[2])

        tank.reparentTo(render)

        #seq = self.charTournant(tank,tour)
        seq = self.avanceChar(tank,pos,newPos)

        return tank,seq

    def avanceChar(self,tank,pos,newPos):
        angle = 0

        dist = newPos[0] - pos[0]
        
        if dist > 0:
            angle = -90
        else:
            angle = 90

        tank.setH(angle)

        dureeSeq = 1.0

        intervalAvance = tank.posInterval(dureeSeq,newPos,startPos = pos, blendType= 'easeOut')
        intervalFonction = Func(self.tournerChar,tank)

        seqAvance = Sequence(intervalAvance,intervalFonction)
        seqAvance.start()

        return seqAvance

    def tournerChar(self,tank):

        if tank == self.tank1:
            self.seq1 = self.charTournant(tank,360)
            

        elif tank == self.tank2:
            self.seq2 = self.charTournant(tank,-360)


    def connecterJoueur1(self):
        nom = self.entreeNom1.get()
        mdp = self.entreePass1.get()
        #mdp = self.pass1

        joueur = self.dao.readJoueur(nom,mdp)
        if joueur != None:
            if self.dto.joueur2 != None and joueur.nom == self.dto.joueur2.nom:
                self.entreeMess.set("Erreur joueur déjà connecté!")  
            else:
                self.dto.joueur1 = joueur
                
                self.tank1,self.seq1 = self.creerChar(joueur,(-10, 20, -3),(-5, 20, -3))

                self.showText()
                self.entreeMess.set("Joueur1 connecté!")
        else:
            self.entreeMess.set("Erreur joueur1!")  

    def connecterJoueur2(self):
        nom = self.entreeNom2.get()
        mdp = self.entreePass2.get()
      #  mdp = self.pass2

        joueur = self.dao.readJoueur(nom,mdp)
        if joueur != None:
            if self.dto.joueur1 != None and joueur.nom == self.dto.joueur1.nom:
                self.entreeMess.set("Erreur joueur déjà connecté!") 
            else:
                self.dto.joueur2 = joueur

                self.tank2,self.seq2 = self.creerChar(joueur,(10, 20, -3),(6, 20, -3))

                self.showText()
                self.entreeMess.set("Joueur2 connecté!")
        else:
            self.entreeMess.set("Erreur joueur2!")            

    def showText(self):
        if(self.dto.joueur1 != None and self.dto.joueur2 != None):
            self.textObject5.setText(AnalyseDTOJoueur.calculTitre(self.dto.joueur1))
            self.textObject7.setText(AnalyseDTOJoueur.calculTitre(self.dto.joueur2))

            favori = "Aucun joueur favori"

            if(self.dto.joueur1.niveau > self.dto.joueur2.niveau):
                favori = AnalyseDTOJoueur.calculTitre(self.dto.joueur1)+" est favori"
            elif(self.dto.joueur2.niveau > self.dto.joueur1.niveau):
                favori = AnalyseDTOJoueur.calculTitre(self.dto.joueur2)+" est favori"

            self.textObject8.setText(favori)

            self.textObject6.show()
            self.b1.show()
            self.textObject9.show()

    def loadDefaultTank(self):
            r = randint(0,255)
            g = randint(0,255)
            b = randint(0,255)

            f = randint(0,20)
            a = randint(0,20)
            d = randint(0,20)
            v = randint(0,20)
            self.dto.joueur1 = Joueur("DEFAUT1",(r/255.0,g/255.0,b/255.0),(v,f,a,d),5000,1)

            self.tank1,self.seq1 = self.creerChar(self.dto.joueur1,(-10, 20, -3),(-5, 20, -3))

            r = randint(0,255)
            g = randint(0,255)
            b = randint(0,255)

            f = randint(0,20)
            a = randint(0,20)
            d = randint(0,20)
            v = randint(0,20)
            self.dto.joueur2 = Joueur("DEFAUT2",(r/255.0,g/255.0,b/255.0),(v,f,a,d),5000,1)

            self.tank2,self.seq2 = self.creerChar(self.dto.joueur2,(10, 20, -3),(6, 20, -3))

            self.showText()

    def launchGame(self):
        #Launch Sequence    
        Sequence(
                Func(lambda : self.cacher()),
                Func(lambda : self.transition.irisOut(0.2)),
                SoundInterval(self.sound),
                Func(lambda : messenger.send("DemarrerPartie")),   
                Wait(0.2),             
                Func(lambda : self.transition.irisIn(0.2))
        ).start()
