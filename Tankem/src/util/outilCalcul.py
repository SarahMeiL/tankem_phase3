# -*- coding: utf-8 -*-
from random import randint

class AnalyseDTOJoueur(object):
	def calculTitre(joueur):
		titre = joueur.nom

		paliers = [10,5,1]
		lib_titre = {"Vie":{str(paliers[2]):{"A":"le fougeux","B":"fougueux"},
						str(paliers[1]):{"A":"le pétulant","B":"pétulant"},
						str(paliers[0]):{"A":"l'immortel","B":"immortel"}
						},
					"Force":{str(paliers[2]):{"A":"le crossfiter","B":"qui fait du crossfit"},
						str(paliers[1]):{"A":"le hulk","B":"brutal"},
						str(paliers[0]):{"A":"le tout puissant","B":"Marc-André"}
						},
					"Agilite":{str(paliers[2]):{"A":"le prompt","B":"prompt"},
						str(paliers[1]):{"A":"le lynx","B":"lynx"},
						str(paliers[0]):{"A":"le foudroyant","B":"foudroyant"}
						},
					"Dexterite":{str(paliers[2]):{"A":"le précis","B":"précis"},
						str(paliers[1]):{"A":"l'habile","B":"habile"},
						str(paliers[0]):{"A":"le chirurgien","B":"chirurgien"}
						}
					}

		vie = joueur.cool_point.vie
		force = joueur.cool_point.force
		agilite = joueur.cool_point.agilite
		dexterite = joueur.cool_point.dexterite
		maxStat = 20

		if vie == maxStat and force == maxStat and agilite == maxStat and dexterite == maxStat:
			return joueur.nom +" Dominateur"

		stats = {"Vie":vie,"Force":force,"Agilite":agilite,"Dexterite":dexterite}

		nb_stats = 0

		for key in stats:
			s = stats[key]
			if s > 0:
				nb_stats += 1

		if nb_stats > 0:
			meilleur_stat = AnalyseDTOJoueur.__getQualificatif(stats)
			palier = str(AnalyseDTOJoueur.__testQualificatif(meilleur_stat[1],paliers))
			titre += " "+lib_titre[meilleur_stat[0]][palier]["A"]

			if nb_stats > 1:
				deuxieme_meilleur = AnalyseDTOJoueur.__getQualificatif(stats,meilleur_stat[0])
				palier = str(AnalyseDTOJoueur.__testQualificatif(deuxieme_meilleur[1],paliers))
				titre += " "+lib_titre[deuxieme_meilleur[0]][palier]["B"]

		return titre

		pass

	def __getQualificatif(stats,meilleur_stat=None):
		deuxieme_meilleur = None
		egal = []

		for key in stats:
			s = stats[key]
			if key != meilleur_stat:
				if deuxieme_meilleur == None:
					deuxieme_meilleur = (key,s)
					egal.append((key,s))

				elif deuxieme_meilleur[1] < s:
					deuxieme_meilleur = (key,s)
					egal = []
					egal.append((key,s))

				elif deuxieme_meilleur[1] == s:
					egal.append((key,s))

		if len(egal) > 1:
			i = randint(0,(len(egal)-1))
			deuxieme_meilleur = egal[i]

		return deuxieme_meilleur

	def __testQualificatif(stat,paliers):
		for p in paliers:
			if stat >= p:
				return p
		return paliers[0]

	def calculExperience(joueur,gagnant,favoris):
		f = 1
		if favoris == joueur:
			f = 0

		if gagnant == joueur:
			return 100 + 100*f + gagnant.vie*2
		else:
			return 2* (gagnant.vie_max - gagnant.vie)


	def calculStats(joueur,balanceDTO):
		tempBase = {}

		tempBase["Vie"] = balanceDTO.numData["tank_speed"]
		tempBase["Force"] = 10
		tempBase["Agilite"] = 5
		tempBase["Dexterite"] = 4

		vie = joueur.cool_point.vie
		force = joueur.cool_point.force
		agilite = joueur.cool_point.agilite
		dexterite = joueur.cool_point.dexterite

		stats = {}

		stats["Vie"] = tempBase["Vie"] + (tempBase["Vie"]*(5.0/100.0))*vie
		stats["Force"] = tempBase["Force"] + (tempBase["Force"]*(5.0/100.0))*force
		stats["Agilite"] = tempBase["Agilite"] + (tempBase["Agilite"]*(2.5/100.0))*agilite
		stats["Dexterite"] = tempBase["Dexterite"] + (tempBase["Dexterite"]*(2.5/100.0))*dexterite

		return stats

	def calculStat(joueur,attribut,type):
		val = {"Vie":0.05,"Force":0.05,"Agilite":0.025,"Dexterite":0.025}


		if type in val:
			point = 0

			if(type == "Vie"):
				point = joueur.cool_point.vie
			elif(type == "Force"):
				point = joueur.cool_point.force
			elif(type == "Agilite"):
				point = joueur.cool_point.agilite
			elif(type == "Dexterite"):
				point = joueur.cool_point.dexterite

			return attribut + (attribut*val[type])*point
		else:
			return attribut


	def testLevelUp(joueur):
		seuil = 100*(joueur.niveau + 1) + 50* joueur.niveau**2
		if (seuil - joueur.experience) <= 0:
			return True
		else:
			return False

	def testLevelUp(exp,niveau):
		seuil = 100*(niveau + 1) + 50* niveau**2
		if (seuil - exp) <= 0:
			return True
		else:
			return False

	def calculNiveau(exp):
		niveau = 0

		while AnalyseDTOJoueur.testLevelUp(exp,niveau):
			niveau += 1;

		return niveau
		

	testLevelUp = staticmethod(testLevelUp)
	calculExperience = staticmethod(calculExperience)
	calculStats = staticmethod(calculStats)
	__testQualificatif = staticmethod(__testQualificatif)
	__getQualificatif = staticmethod(__getQualificatif)
	calculTitre = staticmethod(calculTitre)
	calculStat = staticmethod(calculStat)
	calculNiveau = staticmethod(calculNiveau)
