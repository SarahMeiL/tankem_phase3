# -*- coding: utf-8 -*-
from __future__ import print_function
import Tkinter
import tkMessageBox 
from constants import *

class Sanitizer(object):
	"""Sanitizer servant a verifier les erreurs d'insertion"""
	def __init__(self):	
		self.errors = []

	def isValid(self,targetDict,insertedData):
		if insertedData not in targetDict:	
			self.errors.append("[Erreur de donnée] '"+insertedData+"' n'est pas présent dans les paramètres du jeu.")	
			return False
		else:			
			return True

	def isNumber(self,paramName,insertedNumber):
		try:		
			val = float(insertedNumber)
			return True
		except ValueError:		
			self.errors.append("[Erreur de donnée] '"+paramName+"', la valeur '"+insertedNumber+"' n'est pas une valeur numérique.")
			return False	

	def showErrors(self,dto):	
		#Separator
		self.errors.append("="*47)	

		#Insert missing game parameters
		for (key,value) in dto.numData.iteritems():
			if key+GET_ID not in dto.numData and (GET_ID not in key) and (GET_MIN not in key) and (GET_MAX not in key):
				self.errors.append("[Paramètre de jeu] '"+key+"' non-inséré, utilisation de la valeur par défaut")
		
		for (key,value) in dto.textData.iteritems():
			if key+GET_ID not in dto.textData and (GET_ID not in key) and (GET_MIN not in key) and (GET_MAX not in key):
				self.errors.append("[Paramètre de jeu] '"+key+"' non-inséré, utilisation de la valeur par défaut")		

		#Insert data errors
		if len(self.errors) > 1:			
			Tkinter.Tk().withdraw()
			log = ""
			for e in self.errors:
				log+=e+"\n\n"
			tkMessageBox.showinfo("ERREURS", log)		

