# -*- coding: utf-8 -*-
from __future__ import print_function
from BaseDAO import BaseDAO
from Singleton_connexion import Singleton_connexion
import cx_Oracle
from Joueur import Joueur
import sys,os
fileDir = os.path.dirname(os.path.abspath(__file__))   # Directory of the Module
print(fileDir)
parentDir = os.path.dirname(fileDir) 
sys.path.append(parentDir+"\util")

from outilCalcul import *

class DAO_Joueur(BaseDAO):
	"""DAO servant  à travailler avec la table joueur"""
	def __init__(self):	
		super(DAO_Joueur,self).__init__()
		self.instance_connexion = Singleton_connexion()
		self.curseur = self.instance_connexion.connexion.cursor()

	def readJoueur(self,nom,mdp):
		column = self.curseur.var(cx_Oracle.CURSOR)

		column = self.curseur.callproc("authenticate_user",[nom,mdp,column])
		column = column[-1].fetchall()
		if len(column) > 0:
			column = column[0]

			return Joueur(column[0],column[1],column[2],(round(int(column[4])/255.0,3),round(int(column[5])/255.0,3),round(int(column[6])/255.0,2)),(column[8],column[9],column[10],column[11]),column[7],AnalyseDTOJoueur.calculNiveau(column[7]),column[3])
		else:
			return None
		pass

	def updateJoueur(self,DTO):
		DTO.joueur1 = self.readJoueur(DTO.joueur1.nom,DTO.joueur1.mdp)
		DTO.joueur2 = self.readJoueur(DTO.joueur2.nom,DTO.joueur2.mdp)
