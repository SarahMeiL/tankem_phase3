# -*- coding: utf-8 -*-

from BaseDAO import BaseDAO
from DAO_Balance_Oracle import Balance_Oracle_DAO
from BalanceDTO import Balance_DTO
import csv
from constants import *

class Balance_CSV_DAO(BaseDAO):
	"""DAO servant aux accès au fichier CSV"""
	def __init__(self):	
		super(Balance_CSV_DAO,self).__init__()	

	########################################################################
	# Methodes internes
	########################################################################
	def getRowsFromDict(self,targetTable,currDict,buffer):
		if targetTable:			
				
			#Insert the header for the current dict
			buffer.append(())
			buffer.append(("TYPE","ID",targetTable.upper(),"VALUE","MIN","MAX"))

			for (name,value) in currDict.iteritems():	
				if targetTable == NUMERIC_TABLE:
					dataType = DATATYPE_NUM
				else:
					dataType = DATATYPE_TXT
				#Only get the attribute name and get the corresponding values afterward
				if( (GET_ID not in name) and (GET_MIN not in name) and (GET_MAX not in name)):
					#If it's a game parameter that wasn't updated, show it as MISSING
					if(name+GET_ID not in currDict):
						dataType = "MISSING"
						tmpID = tmpMax = tmpMin = "---"
					else:										
						tmpID = currDict[name+GET_ID]
						tmpMax = currDict[name+GET_MAX]
						tmpMin = currDict[name+GET_MIN]
					buffer.append((dataType,tmpID,name,value,tmpMin,tmpMax))
		
	########################################################################
	# Methodes héritées
	########################################################################	
	def getGameAttributes(self,balanceDTO,targetFile):
		isValid = True
		with open(targetFile,'r') as f:
			reader = csv.reader(f,delimiter=';')
			

			# Only read valid lines
			for row in reader:				
				if(len(row) != 0 and (row[0] == DATATYPE_NUM or row[0] == DATATYPE_TXT)):					
					tmpType = row[0]
					tmpID = row[1]
					tmpName = row[2]
					if tmpType == DATATYPE_NUM:
						tmpVal = float(row[3])
					else:
						tmpVal = row[3]
					tmpMin = float(row[4])
					tmpMax = float(row[5])

					if(self.isNumber(tmpName,row[4]) and self.isNumber(tmpName,row[5])):						
						if tmpType == DATATYPE_NUM:
							balanceDTO.numData[tmpName] = tmpVal
							balanceDTO.numData[tmpName+GET_MIN] = tmpMin
							balanceDTO.numData[tmpName+GET_MAX] = tmpMax
							balanceDTO.numData[tmpName+GET_ID] = tmpID
						elif tmpType == DATATYPE_TXT:
							balanceDTO.textData[tmpName] = tmpVal
							balanceDTO.textData[tmpName+GET_MIN] = tmpMin
							balanceDTO.textData[tmpName+GET_MAX] = tmpMax
							balanceDTO.textData[tmpName+GET_ID] = tmpID
					else:
						isValid = False

		#If we reach this point, we assume that everything is valid
		self.showErrors(balanceDTO)			
		return isValid

	def updateGameAttributes(self,balanceDTO,targetFile):
		buffer = []
		#Fill up the buffer
		self.getRowsFromDict(NUMERIC_TABLE,balanceDTO.numData,buffer)
		self.getRowsFromDict(TEXT_TABLE,balanceDTO.textData,buffer)

		#Write the data in a CSV
		with open(targetFile,'wb') as f:       
			w = csv.writer(f,delimiter=";")   
			for (row) in buffer:
				w.writerow(row) 
									
		f.close()       		
