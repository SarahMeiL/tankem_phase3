# -*- coding: utf-8 -*-
from datetime import datetime

class DTO_Statistique(object):
	def __init__(self):
		self.temps_debut_partie = 0
		self.temps_fin_partie = 0;
		self.id_map = None
		self.liste_arme = []
		self.liste_tank = []
		self.joueur1 = None
		self.joueur2 = None
		self.joueur_gagnant =None

class Stat_joueur(object):
	def __init__(self):
		self.nom
		self.niveau
		self.distance_parcouru
		self.vie_restante

# class Stat_arme(object):
# 	def __init__(self):
# 		self.nom_arme
# 		self.nom_joueur
# 		self.nb_coup_total
# 		self.nb_coup_atteint
# 		self.nb_coup_recu

class Log_arme(object):
	def __init__(self,joueur,arme,tire=False,touche=False,recu=False):
		self.nom_joueur = joueur
		self.nom_arme = arme
		self.a_tire = tire
		self.a_touche = touche
		self.a_recu = recu
