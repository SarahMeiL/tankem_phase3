# -*- coding: utf-8 -*-

DB_PASSWORD = "A"
DB_USERLOGIN = "e1586848"
DB_HOST = "delta"
DB_SID = "decinfo"
PATH_ORACLE = 'C:\Oracle\Client\product\12.2.0\client_1\bin'

#Indexes of the different columns
ATTRIBUTE_ID = 0
ATTRIBUTE_NAME = 1
ATTRIBUTE_VALUE = 2
ATTRIBUTE_MIN = 3
ATTRIBUTE_MAX = 4

#Names of the different columns
COL_VALUE = "val"
COL_MIN = "min"
COL_MAX = "max"

NUMERIC_TABLE = "attributs"
GET_ID = "_ID"
GET_MIN = "_MIN"
GET_MAX = "_MAX"
TEXT_TABLE = "messages"
CSV_FILE = "test.csv"
DATATYPE_NUM = "NUM"
DATATYPE_TXT = "TXT"