# -*- coding: utf-8 -*-
import cx_Oracle
from constants import *
import sys

class Singleton(object):
	__shared_dict = {}

	def __init__(self):
		self.__dict__ = Singleton.__shared_dict

class Singleton_connexion(Singleton):
	def __init__(self):
		Singleton.__init__(self)
		self.init_attribute()
	
	def init_attribute(self):
		if not hasattr(self,"connexion"):
			try:
				# Init de la connection	
				sys.path.append(PATH_ORACLE)    
				dsn_tns = cx_Oracle.makedsn(DB_HOST, 1521, DB_SID)
				chaineConnexion = DB_USERLOGIN + '/' + DB_PASSWORD + '@' + dsn_tns   
		
				# Assignation des variables
				self.connexion = cx_Oracle.connect(chaineConnexion)
				self.cur = self.connexion.cursor()	
			except:
				print("ERREUR!")

	




