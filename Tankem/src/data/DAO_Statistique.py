# -*- coding: utf-8 -*-
from __future__ import print_function
from BaseDAO import BaseDAO
from Singleton_connexion import Singleton_connexion
from datetime import datetime
import cx_Oracle

class DAO_Statistique(BaseDAO):
	"""DAO servant  à travailler avec la table statistique"""
	class __Stat_arme(object):
		"""classe servant a regrouper tous les coups tiré d'une arme durant la partie"""
		def __init__(self,nom_joueur,nom_arme):
			self.nom_joueur = nom_joueur
			self.nom_arme = nom_arme
			self.nb_coup_total = 0
			self.nb_coup_touche = 0
			self.nb_coup_recu = 0

		def __str__(self):
			return self.nom_joueur + " - "+self.nom_arme + " - coup tire: "+str(self.nb_coup_total)+", coup touche: "+str(self.nb_coup_touche)+", coup recu: "+str(self.nb_coup_recu);


	def __init__(self):
		super(DAO_Statistique,self).__init__()	
		self.instance_connexion = Singleton_connexion()
		self.curseur = self.instance_connexion.connexion.cursor()

	def envoyer_statistique(self,DTO_Statistique):

		tempDebut = DTO_Statistique.temps_debut_partie
		tempDebut = str(tempDebut.year)+"-"+str(tempDebut.month)+"-"+str(tempDebut.day)+" "+str(tempDebut.hour)+":"+str(tempDebut.minute)+":"+str(tempDebut.second)

		tempFin = DTO_Statistique.temps_fin_partie
		tempFin = str(tempFin.year)+"-"+str(tempFin.month)+"-"+str(tempFin.day)+" "+str(tempFin.hour)+":"+str(tempFin.minute)+":"+str(tempFin.second)


		stat_arme = {}

		for arme in DTO_Statistique.liste_arme:
			nom_joueur = DTO_Statistique.liste_tank[arme.lanceurId].nom
			nom_cible = arme.toucherId
			nom_arme = str(arme.typeBalle)

			if(nom_cible != None):
				nom_cible = DTO_Statistique.liste_tank[arme.toucherId].nom


			if not nom_joueur in stat_arme:
				stat_arme[nom_joueur] = {}
			
			if not nom_arme in stat_arme[nom_joueur]:
				stat_arme[nom_joueur][nom_arme] = DAO_Statistique.__Stat_arme(nom_joueur,nom_arme)

			stat_arme[nom_joueur][nom_arme].nb_coup_total += 1

			if(nom_cible != None):
				if not nom_cible in stat_arme:
					stat_arme[nom_cible] = {}

				if not nom_arme in stat_arme[nom_cible]:
					stat_arme[nom_cible][nom_arme] = DAO_Statistique.__Stat_arme(nom_cible,nom_arme)

				stat_arme[nom_joueur][nom_arme].nb_coup_touche += 1
				stat_arme[nom_cible][nom_arme].nb_coup_recu += 1


		gagnant = None
		perdant = None

		if DTO_Statistique.joueur_gagnant == DTO_Statistique.joueur1.nom:
			gagnant = DTO_Statistique.joueur1
			perdant = DTO_Statistique.joueur2
		else:
			gagnant = DTO_Statistique.joueur2
			perdant = DTO_Statistique.joueur1

		g_dist = 0
		p_dist = 0
		g_vie = 0

		for tank in DTO_Statistique.liste_tank:
			if tank.nom == gagnant.nom:
				g_dist = tank.dist
				g_vie = tank.pointDeVie
			else:
				p_dist = tank.dist

		id_partie = self.curseur.var(cx_Oracle.NUMBER)
		id_partie = self.curseur.callproc("partie_terminee",[DTO_Statistique.id_map,gagnant.id,g_dist,perdant.id,p_dist,tempDebut,tempFin,g_vie,id_partie])

		id_partie = id_partie[-1]

		liste_id_arme = {"Canon":6,
						"Grenade":5,
						"Mitraillette":1,
						"Piege":3,
						"Shotgun":2,
						"Guide":4}

		for joueur in stat_arme:
			for arme in stat_arme[joueur]:
				id_joueur = 0
				id_arme = liste_id_arme[arme]
				nb_recu = stat_arme[joueur][arme].nb_coup_recu
				nb_tire = stat_arme[joueur][arme].nb_coup_total
				nb_touche = stat_arme[joueur][arme].nb_coup_touche

				if joueur == gagnant.nom:
					id_joueur = gagnant.id
				else:
					id_joueur = perdant.id
				column = self.curseur.var(cx_Oracle.NCHAR)
				column = self.curseur.callproc("joueur_arme_statistiques",[id_partie,id_arme,id_joueur,nb_touche,nb_recu,nb_tire,column])
				
	

	