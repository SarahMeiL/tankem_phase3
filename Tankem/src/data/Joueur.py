# -*- coding: utf-8 -*-

class Joueur(object):
	def __init__(self,id,nom,mdp,couleur,cool_point,experientce=0,niveau=1,phrase=""):
		self.id = id
		self.nom = nom
		self.mdp = mdp
		self.couleur = couleur
		self.cool_point = CoolPoint(cool_point[0],cool_point[1],cool_point[2],cool_point[3])
		self.experientce = experientce
		self.niveau = niveau
		self.phrase = phrase

	def __str__(self):
		return "Nom:"+str(self.nom) +" Lv."+str(self.niveau)+" Exp:"+str(self.experientce)+" Couleur:("+str(self.couleur[0])+","+str(self.couleur[1])+","+str(self.couleur[2])+") "+"Cool points:("+str(self.cool_point)+")"



class CoolPoint(object):
	def __init__(self,vie,force,agilite,dexterite):
		self.vie = vie
		self.force = force
		self.agilite = agilite
		self.dexterite = dexterite 

	def __str__(self):
		return "HP:"+str(self.vie)+" FORCE:"+str(self.force)+" AGILITE:"+str(self.agilite)+" DEXTERITE:"+str(self.dexterite)
