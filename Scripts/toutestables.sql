--FIN DU SCRIPT JOUEUR
--CREATION DE LA TABLE PARTIE


--CREATION DE LA VUE DES PARTIES DES JOUEURS
CREATE OR REPLACE VIEW vue_parties AS
  SELECT partie.id AS "partie", gagnant.nom AS "gagnant",
    gagnant.id AS "gagnant_id",
    distancegagnant.distance AS "distance_gagnant",
    perdant.nom AS "perdant",
    perdant.id AS "perdant_id",
    distanceperdant.distance AS "distance_perdant",
    niveau.id AS "map_id",
    niveau.nom AS "map",
    partie.timestamp_debut AS "debut",
    partie.timestamp_fin AS "fin"
  FROM partie
    INNER JOIN joueur gagnant
      ON gagnant.id = partie.gagnant
    INNER JOIN distance_parcourue distancegagnant
      ON distancegagnant.joueur = gagnant.id AND distancegagnant.partie = partie.id
    INNER JOIN joueur perdant
      ON perdant.id = partie.perdant
    INNER JOIN distance_parcourue distanceperdant
      ON distanceperdant.joueur = perdant.id AND distanceperdant.partie = partie.id
    LEFT JOIN niveau
      ON niveau.id = partie.map_jouee;
select * from vue_parties;
select * from joueur;
--CREATION DE LA TABLE DISTACE_PARCOURUE
DROP TABLE distance_parcourue CASCADE CONSTRAINTS;
CREATE TABLE distance_parcourue(
  partie NUMBER(6) NOT NULL,
  joueur NUMBER(5) NOT NULL,
  distance NUMBER(8,2) NOT NULL,
  CONSTRAINT fk_distance_parcourue_joueur FOREIGN KEY (joueur) REFERENCES joueur(id) ON DELETE CASCADE,
  CONSTRAINT fk_distance_parcourue_partie FOREIGN KEY (partie) REFERENCES partie(id) ON DELETE CASCADE,
  CONSTRAINT cc_distance_parcourue_distance CHECK (distance >= 0),
  CONSTRAINT pk_distance_parcourue PRIMARY KEY (partie, joueur)
);

--CR�ATION DE LA TABLE PARTIE_JOUEUR_ARME
DROP TABLE partie_joueur_arme CASCADE CONSTRAINTS;
CREATE TABLE partie_joueur_arme(
  count_hit_by NUMBER(3) NOT NULL,
  count_has_hit NUMBER(3) NOT NULL,
  count_used NUMBER(3) NOT NULL,
  partie NUMBER(6) NOT NULL,
  joueur NUMBER(5) NOT NULL,
  arme NUMBER(3) NOT NULL,
  CONSTRAINT fk_partiejoueurarme_joueur FOREIGN KEY (joueur) REFERENCES joueur(id) ON DELETE CASCADE,
  CONSTRAINT fk_partiejoueurarme_partie FOREIGN KEY (partie) REFERENCES partie(id) ON DELETE CASCADE,
  CONSTRAINT fk_partiejoueurarme_arme FOREIGN KEY (arme) REFERENCES arme(id) ON DELETE CASCADE,
  CONSTRAINT pk_partiejoueurarme PRIMARY KEY (partie, joueur, arme)
);

CREATE OR REPLACE VIEW vue_partie_joueur_arme AS
  SELECT joueur.id AS "id_joueur",
         joueur.nom AS "joueur",
         partie AS "id_partie",
         arme.id AS "arme_id",
         arme.nom AS "arme",
         partie_joueur_arme.count_has_hit AS "has_hit",
         partie_joueur_arme.count_hit_by AS "hit_by",
         partie_joueur_arme.count_used AS "has_used"
  FROM partie_joueur_arme
    INNER JOIN joueur
      ON joueur.id = partie_joueur_arme.joueur
    INNER JOIN arme
      ON partie_joueur_arme.arme = arme.id;
SELECT * FROM vue_partie_joueur_arme;

--CREATION DE LA PROCEDURE JOUEUR_ARME_STATISTIQUES
CREATE OR REPLACE PROCEDURE joueur_arme_statistiques(id_partie IN NUMBER, id_arme IN NUMBER, id_joueur IN NUMBER, nb_has_hit NUMBER, nb_hit_by NUMBER, nb_used NUMBER, code_output OUT VARCHAR2)
IS
  nb NUMBER;
BEGIN
  SELECT COUNT(*) INTO nb FROM partie WHERE id = id_partie;
  IF nb <= 0 THEN
    code_output := 'GAME_NOT_FOUND_ID_INVALID';
  ELSE
    INSERT INTO partie_joueur_arme (partie, arme, joueur, count_has_hit, count_hit_by, count_used) VALUES (id_partie, id_arme, id_joueur, nb_has_hit, nb_hit_by, nb_used);
    code_output := 'GAME_FOUND_SUCCESFULLY_INSERTED';
    COMMIT;
  END IF;
END;
/
CREATE OR REPLACE PROCEDURE joueur_arme_stat_aucun_retour(id_partie IN NUMBER, id_arme IN NUMBER, id_joueur IN NUMBER, nb_has_hit NUMBER, nb_hit_by NUMBER, nb_used NUMBER)
IS
  nb NUMBER;
BEGIN
  SELECT COUNT(*) INTO nb FROM partie WHERE id = id_partie;
  IF nb > 0 THEN
    INSERT INTO partie_joueur_arme (partie, arme, joueur, count_has_hit, count_hit_by, count_used) VALUES (id_partie, id_arme, id_joueur, nb_has_hit, nb_hit_by, nb_used);
  END IF;
END;
/
CREATE OR REPLACE FUNCTION niveau_xp(xp NUMBER) RETURN NUMBER
IS
  niveau_joueur_evalue NUMBER(3);
  res_calcul_seuil_xp NUMBER(10);
BEGIN
  niveau_joueur_evalue := 0;
  res_calcul_seuil_xp := 100*(niveau_joueur_evalue+1)+(50*POWER(niveau_joueur_evalue,2));
  WHILE res_calcul_seuil_xp <= xp
  LOOP
    niveau_joueur_evalue:=(niveau_joueur_evalue+1);
    res_calcul_seuil_xp := 100*(niveau_joueur_evalue+1)+(50*POWER(niveau_joueur_evalue,2));
  END LOOP;
  RETURN niveau_joueur_evalue;
END;
/
CREATE OR REPLACE PROCEDURE obtenir_niveau_selon_xp(xp IN NUMBER, niveau OUT NUMBER)
IS
  niveau_joueur_evalue NUMBER(3);
  res_calcul_seuil_xp NUMBER(10);
BEGIN
  niveau_joueur_evalue := 0;
  res_calcul_seuil_xp := 100*(niveau_joueur_evalue+1)+(50*POWER(niveau_joueur_evalue,2));
  WHILE res_calcul_seuil_xp <= xp
  LOOP
    niveau_joueur_evalue:=(niveau_joueur_evalue+1);
    res_calcul_seuil_xp := 100*(niveau_joueur_evalue+1)+(50*POWER(niveau_joueur_evalue,2));
  END LOOP;
  niveau := niveau_joueur_evalue;
END;
/
--CREATION DE LA PROCEDURE DE MISE A JOUR DES COOLPOINTS
CREATE OR REPLACE PROCEDURE set_joueur(id_joueur IN NUMBER, red IN NUMBER, green IN NUMBER, blue IN NUMBER, pts_vie IN NUMBER, pts_agilite IN NUMBER, pts_dexterite IN NUMBER, pts_force IN NUMBER, phrase_joueur IN VARCHAR2, code_output OUT VARCHAR2)
IS
  nb NUMBER(2);
  coolpoints_total NUMBER(3);
  xp_joueur NUMBER(10);
  niveau_joueur NUMBER(3);
  coolpoints_max NUMBER(2);
BEGIN
  code_output := 'ERRORS';
  SELECT COUNT(*) INTO nb FROM joueur WHERE id = id_joueur;
  IF nb > 0 THEN
    SELECT xp INTO xp_joueur FROM joueur WHERE id = id_joueur;
    obtenir_niveau_selon_xp(xp_joueur, niveau_joueur);--OBTENIR LE NIVEAU DU JOUEUR A PARTIR DE SON XP
    --niveau_joueur := niveau_xp(xp_joueur);
    coolpoints_max := 5*niveau_joueur;--CALCULER LE NOMBRE DE COOLPOINTS MAXIMAL
    coolpoints_total := (pts_agilite + pts_dexterite + pts_vie + pts_force);
    IF coolpoints_total <= coolpoints_max THEN
      IF red < 255 OR green < 255 OR blue < 255 THEN
        UPDATE joueur SET
          tank_r = red,
          tank_g = green,
          tank_b = blue,
          phrase = phrase_joueur,
          coolpoints_vie = pts_vie,
          coolpoints_agilite = pts_agilite,
          coolpoints_dexterite = pts_dexterite,
          coolpoints_force = pts_force
        WHERE id = id_joueur;
        code_output := 'SUCCESS';
      ELSE
        code_output := CONCAT(code_output, ' COLOR_INVALID');
      END IF;
    ELSE
      code_output := CONCAT(code_output, ' COOLPOINTS_INVALID');
    END IF;
  ELSE
    code_output := 'USER_NOT_FOUND';
  END IF;
END;
/
CREATE OR REPLACE FUNCTION get_max_hp_joueur(id_joueur NUMBER) RETURN NUMBER
IS
  base_hp NUMBER;
  hp NUMBER;
  coolpoints NUMBER;
BEGIN
  SELECT coolpoints_vie INTO coolpoints FROM joueur WHERE id = id_joueur;
  SELECT val INTO base_hp FROM attributs WHERE name = 'tank_health';
  hp := base_hp + (0.05 * base_hp * coolpoints);
  RETURN hp;
END;
/
--CREATION DE LA PROCEDURE PARTIE_TERMINEE
CREATE OR REPLACE PROCEDURE partie_terminee(id_map IN NUMBER, id_joueur_gagnant IN NUMBER, distance_gagnant IN NUMBER, id_joueur_perdant IN NUMBER, distance_perdant IN NUMBER, debut_string IN VARCHAR2, fin_string IN VARCHAR2, hp_restant_gagnant IN NUMBER, id_partie OUT NUMBER)
IS
  fin TIMESTAMP;
  debut TIMESTAMP;
  xp_gagnant NUMBER;
  niveau_gagnant NUMBER;
  xp_perdant NUMBER;
  niveau_perdant NUMBER;
  xp_ajoutee_gagnant NUMBER;
  xp_ajoutee_perdant NUMBER;
  favori NUMBER;
BEGIN
  fin := TO_TIMESTAMP(fin_string, 'yyyy/mm/dd hh24:mi:ss');
  debut := TO_TIMESTAMP(debut_string, 'yyyy/mm/dd hh24:mi:ss');
  --PARTIE EXPERIENCE
  favori := 0;
  SELECT xp INTO xp_gagnant FROM joueur WHERE id = id_joueur_gagnant;
  SELECT xp INTO xp_perdant FROM joueur WHERE id = id_joueur_perdant;
  obtenir_niveau_selon_xp(xp_gagnant, niveau_gagnant);
  obtenir_niveau_selon_xp(xp_perdant, niveau_perdant);
  IF niveau_gagnant > niveau_perdant THEN
    favori := 1;
  END IF;
  xp_ajoutee_gagnant := 100+(100*favori)+2*hp_restant_gagnant;
  xp_ajoutee_perdant := 2*get_max_hp_joueur(id_joueur_gagnant)-hp_restant_gagnant;
  INSERT INTO partie (map_jouee, gagnant, perdant, timestamp_debut, timestamp_fin) VALUES (id_map, id_joueur_gagnant, id_joueur_perdant, debut, fin);
  SELECT id INTO id_partie
    FROM partie
    WHERE gagnant = id_joueur_gagnant
    AND perdant = id_joueur_perdant
    AND timestamp_debut = debut
    AND timestamp_fin = fin;
  INSERT INTO distance_parcourue (partie, joueur, distance) VALUES (id_partie, id_joueur_gagnant, distance_gagnant);
  INSERT INTO distance_parcourue (partie, joueur, distance) VALUES (id_partie, id_joueur_perdant, distance_perdant);
  
  UPDATE joueur SET timestamp_derniere_partie = debut WHERE id = id_joueur_gagnant OR id = id_joueur_perdant;
  UPDATE joueur SET xp = (xp_gagnant + xp_ajoutee_gagnant) WHERE id = id_joueur_gagnant;
  UPDATE joueur SET xp = (xp_perdant + xp_ajoutee_perdant) WHERE id = id_joueur_perdant;
  COMMIT;
END;
/
CREATE OR REPLACE PROCEDURE partie_terminee_id_fourni(id_map IN NUMBER, id_joueur_gagnant IN NUMBER, distance_gagnant IN NUMBER, id_joueur_perdant IN NUMBER, distance_perdant IN NUMBER, debut_string IN VARCHAR2, fin_string IN VARCHAR2, hp_restant_gagnant IN NUMBER, id_partie IN NUMBER)
IS
  fin TIMESTAMP;
  debut TIMESTAMP;
  xp_gagnant NUMBER;
  niveau_gagnant NUMBER;
  xp_perdant NUMBER;
  niveau_perdant NUMBER;
  xp_ajoutee_gagnant NUMBER;
  xp_ajoutee_perdant NUMBER;
  favori NUMBER;
BEGIN
  fin := TO_TIMESTAMP(fin_string, 'yyyy/mm/dd hh24:mi:ss');
  debut := TO_TIMESTAMP(debut_string, 'yyyy/mm/dd hh24:mi:ss');
  --PARTIE EXPERIENCE
  favori := 0;
  SELECT xp INTO xp_gagnant FROM joueur WHERE id = id_joueur_gagnant;
  SELECT xp INTO xp_perdant FROM joueur WHERE id = id_joueur_perdant;
  niveau_gagnant := niveau_xp(xp_gagnant);
  niveau_perdant := niveau_xp(xp_perdant);
  IF niveau_gagnant > niveau_perdant THEN
    favori := 1;
  END IF;
  xp_ajoutee_gagnant := 100+(100*favori)+2*hp_restant_gagnant;
  xp_ajoutee_perdant := 2*get_max_hp_joueur(id_joueur_gagnant)-hp_restant_gagnant;
  INSERT INTO partie (id, map_jouee, gagnant, perdant, timestamp_debut, timestamp_fin) VALUES (id_partie, id_map, id_joueur_gagnant, id_joueur_perdant, debut, fin);
  
  INSERT INTO distance_parcourue (partie, joueur, distance) VALUES (id_partie, id_joueur_gagnant, distance_gagnant);
  INSERT INTO distance_parcourue (partie, joueur, distance) VALUES (id_partie, id_joueur_perdant, distance_perdant);
  
  UPDATE joueur SET timestamp_derniere_partie = debut WHERE id = id_joueur_gagnant OR id = id_joueur_perdant;
  UPDATE joueur SET xp = xp_gagnant + xp_ajoutee_gagnant WHERE id = id_joueur_gagnant;
  UPDATE joueur SET xp = xp_perdant + xp_ajoutee_perdant WHERE id = id_joueur_perdant;
  COMMIT;
END;
/
--INSERTION DES ARMES
INSERT INTO arme (id, nom) VALUES (1, 'mitraillette');
INSERT INTO arme (id, nom) VALUES (2, 'shotgun');
INSERT INTO arme (id, nom) VALUES (3, 'piege');
INSERT INTO arme (id, nom) VALUES (4, 'guide');
INSERT INTO arme (id, nom) VALUES (5, 'grenade');
INSERT INTO arme (id, nom) VALUES (6, 'aucune');
SELECT * FROM arme;
--INSERTION DES JOUEURS BIDON
--INSERT INTO joueur (id, nom, mdp) VALUES (1, 'test_user_1', 'AAAaaa111');
--INSERT INTO joueur (id, nom, mdp) VALUES (2, 'test_user_2', 'AAAaaa111');
--INSERT INTO joueur (id, nom, mdp) VALUES (3, 'test_user_3', 'AAAaaa111');
--SELECT * FROM joueur;

--INSERTION D'UNE PARTIE BIDON
--INSERT INTO partie (id, gagnant, perdant, timestamp_debut, timestamp_fin) VALUES (1, 1, 2, TO_TIMESTAMP('2003/07/09 10:23:19', 'yyyy/mm/dd hh:mi:ss'), TO_TIMESTAMP('2003/07/09 10:25:08', 'yyyy/mm/dd hh:mi:ss'));
--INSERT INTO partie_joueur_arme (joueur, partie, arme, count_used, count_has_hit, count_hit_by) VALUES (1,1,1,0,10,0);
--INSERT INTO partie_joueur_arme (joueur, partie, arme, count_used, count_has_hit, count_hit_by) VALUES (2,1,1,0,0,10);

--REQUETE POUR OBTENIR LES PARTIES D'UN UTILISATEUR
--SELECT * FROM vue_parties WHERE "gagnant_id" = 1 OR "perdant_id" = 1;

--REQUETE POUR OBTENIR LES STATISTIQUES D'UTILISATION D'ARME D'UN JOUEUR
--SELECT * FROM vue_partie_joueur_arme WHERE "id_joueur" = 1;
COMMIT;