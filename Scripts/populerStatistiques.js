const fs = require("fs");
const moment = require("moment");
const gaussian = require("gaussian");
const readline = require("readline");
const clear = require("clear");
const center = require("center-align");

const getConsoleWidth = () => {return process.stdout.columns};
const getConsoleHeight = () => {return process.stdout.rows};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let contenuFichier = "";

const distributionTempsPartie = gaussian(120, 20);
const distributionDistanceParcourue = gaussian(450,500);


function prendreMapAleatoire(obj) {
  let result;
  let count = 0;
  for (let prop in obj)
      if (Math.random() < 1/++count)
         result = prop;
  return result;
}
function compterAttributs(obj) {
  let count = 0;
  for (let prop in obj)
      ++count;
  return count;
}
let armes = [
  "mitraillette",
  "shotgun",
  "piege",
  "guide",
  "spring",
  "aucune",
];
let armeRange = armes.length;
let joueursPossibles = {
  "steve":1,
  "MichaelJackson":2,
  "yowtf":2,
  "AnimeLover300":4,
  "SammyClassicSonicFan":5,
  "_-Slayer-_":6,
  "_-Killer-_":7,
  "Slayer69":8,
  "ElonMusk":9,
  "BillGates":10,
  "AppleFan1":11,
  "xboxlivegamer0909":12,
  "punkAss":13,
  "datboi":14,
  "despacito2":15,
  "nasa":16,
  "spacex":17,
  "idklel":18,
  "allo":19,
  "Masterkiller69":20,
  "DrPhilTM":21,
  "HelloMan":22,
  "LittleFriend":23,
  "Biggie":24,
  "Non":25,
  "molester420":24,
  "yo":24,
  "despacito3ConfirmedByNasa":25,
  "yeet":26,
  "poulet":27,
  "salutbig":28,
  "sayhello":29,
  "Carotte99":30,
  "SteveHarvey":31,
  "MrWorldwide":32,
  "Miami":33,
  "YoWaddup":34,
  "ImDone":35,
  "copypasta":36
};
//let str =  "INSERT INTO partie (gagnant, perdant, date_debut, date_fin) VALUES (1, 21, TO_DATE('2003/07/09', 'yyyy/mm/dd hh:mi:ss'), TO_DATE('2012/07/09', 'yyyy/mm/dd hh:mi:ss'));";
function generer_fichier(nomFichier, dommagesAttaque, nombreParties, nombreJoueurs) {
  const distributionDommagesAttaque = gaussian(dommagesAttaque, 15);

  let joueursInseres = [];
  let mot_de_passe_bidon = "AAAaaa111";
  for(let joueur in joueursPossibles){
    if(Math.random() > 0.5){
      if(joueursInseres.length < nombreJoueurs){
        joueursInseres.push(joueur);
        let str = "INSERT INTO joueur(id, nom, mdp) VALUES("+joueursPossibles[joueur]+","+"'"+joueur+"','"+mot_de_passe_bidon+"');\n"
        console.log(str);
        contenuFichier += str;
      }
    }
  }
  let niveaux = {
    "Blankem":9,
    "Zen garden II":4,
    "Zen garden III":6,
    "Demo2":12,
    "Loss":8,
    "Demo":10,
    "Sans titre":13,
    "DEMO prof":14
  };
  let mapRange = 8;
  let nbParties = nombreParties;
  for (i = 0; i < nbParties; i++) {
    let idPartie = i+1;
    let mapId = niveaux[prendreMapAleatoire(niveaux)];
    //Duree aleatoire entre 30 secs et 6 mins
    let duration = distributionTempsPartie.ppf(Math.random());
    let mins = Math.floor(duration / 60);
    let secs = Math.round(duration - mins * 60);
    let durationStr =
      mins.toString() +
      ":" +
      (secs < 10 ? "0" + secs.toString() : secs.toString());
    //Journee aleatoire, TODAY plus ou moins 300 JOURS
    let journee = moment().add(Math.random() * 6 - 3, "days");
    let debutTimestamp = journee.format("YYYY/MM/DD hh:mm:ss");

    let fin = journee.add(duration, "seconds");
    let finTimestamp = fin.format("YYYY/MM/DD hh:mm:ss");

    //Choix de deux joueur aléatoires
    let idxPlayer1 = Math.floor(Math.random() * joueursInseres.length);
    let player1 = joueursInseres[idxPlayer1];
    let player1Id = joueursPossibles[player1];

    //S'assurer de ne pas prendre 2 fois le même
    let idxPlayer2 = idxPlayer1;

    while (idxPlayer2 == idxPlayer1) {
      idxPlayer2 = Math.floor(Math.random() * joueursInseres.length);
    }
    let player2 = joueursInseres[idxPlayer2];
    let player2Id = joueursPossibles[player2];
    
    let hpPlayer1 = 200;
    let hpPlayer2 = 200;
    
    let insertionsStatistiques = "";
    let idxArmeJoueur1 = Math.floor(Math.random()*armeRange);
    let idxArmeJoueur2 = Math.floor(Math.random()*armeRange);
    let probChangementJoueur = 0.995;
    let armesJoueur1 = {};
    let armesJoueur2 = {};
    while(hpPlayer1 > 0 && hpPlayer2 > 0){
      if(Math.random() > probChangementJoueur){
        idxArmeJoueur1 = Math.floor(Math.random()*armeRange);
      }
      if(Math.random() > probChangementJoueur){
        idxArmeJoueur2 = Math.floor(Math.random()*armeRange);
      }
      let armeJoueur1 = armes[idxArmeJoueur1].toString();
      let armeJoueur2 = armes[idxArmeJoueur2].toString();
      if(Math.random() > 0.5){//joueur 1 tire
        if(armesJoueur1.hasOwnProperty(armeJoueur1)){
          armesJoueur1[armeJoueur1][0]++;
        }
        else{
          armesJoueur1[armeJoueur1] = [0,0,0];
          armesJoueur1[armeJoueur1][0]++;
        }
        if(Math.random()>0.5){//joueur 1 atteint la cible
          armesJoueur1[armeJoueur1][1]++;//count_has_hit
          if(armesJoueur2.hasOwnProperty(armeJoueur1)){
            armesJoueur2[armeJoueur1][2]++;//count_hit_by
          }
          else{
            armesJoueur2[armeJoueur1] = [0,0,1];
          }
        }
        hpPlayer2-=distributionDommagesAttaque.ppf(Math.random());
      }else{
        if(armesJoueur2.hasOwnProperty(armeJoueur2)){
          armesJoueur2[armeJoueur2][0]++;
        }
        else{
          armesJoueur2[armeJoueur2] = [0,0,0];
          armesJoueur2[armeJoueur2][0]++;
        }
        if(Math.random()>0.5){//joueur 1 atteint la cible
          armesJoueur2[armeJoueur2][1]++;//count_has_hit
          if(armesJoueur1.hasOwnProperty(armeJoueur2)){
            armesJoueur1[armeJoueur2][2]++;//count_hit_by
          }
          else{
            armesJoueur1[armeJoueur2] = [0,0,1];
          }
        }
        hpPlayer1-=distributionDommagesAttaque.ppf(Math.random());
      }
      console.log("id: "+player1Id+" "+player2Id)
      console.log("attacc "+hpPlayer1+" "+hpPlayer2);
    }
    for (let arme in armesJoueur2) {
      if (armesJoueur2.hasOwnProperty(arme)) {
        let armeId = idxArmeJoueur2;
        let statArmeJoueur2 = "EXECUTE joueur_arme_stat_aucun_retour("+idPartie+","+armeId+","+player2Id+","+armesJoueur2[arme][1]+","+armesJoueur2[arme][2]+","+armesJoueur2[arme][0]+");\n";
        insertionsStatistiques+=statArmeJoueur2;
      }
    }
    for (let arme in armesJoueur1) {
      if (armesJoueur1.hasOwnProperty(arme)) {
        let armeId = idxArmeJoueur1;
        let statArmeJoueur1 = "EXECUTE joueur_arme_stat_aucun_retour("+idPartie+","+armeId+","+player1Id+","+armesJoueur1[arme][1]+","+armesJoueur1[arme][2]+","+armesJoueur1[arme][0]+");\n";
        insertionsStatistiques+=statArmeJoueur1;
      }
    }
    
    hpGagnant = hpPlayer1 > hpPlayer2 ? hpPlayer1 : hpPlayer2;
    idGagnant = hpPlayer1 > hpPlayer2 ? player1Id : player2Id;
    idPerdant = hpPlayer1 > hpPlayer2 ? player2Id : player1Id;
    let requetePartie = "EXECUTE partie_terminee_id_fourni("+mapId+","+idGagnant+","+Math.floor(distributionDistanceParcourue.ppf(Math.random()))+","+idPerdant+","+Math.floor(distributionDistanceParcourue.ppf(Math.random()))+",'"+debutTimestamp+"','"+finTimestamp+"',"+Math.floor(hpGagnant)+","+idPartie+");\n"
    contenuFichier+=requetePartie;
    contenuFichier+=insertionsStatistiques;
  }
  contenuFichier+="COMMIT;";
  fs.writeFileSync(nomFichier, contenuFichier)
}
clear();
console.log("\n\n"+center("Bienvenue dans le simulateur de partie.\n\nCet utilitaire simulera des parties jouées entre les joueurs générés.\n\nVous pouvez générer un nombre illimité de parties,\njouées par jusqu\'à "+compterAttributs(joueursPossibles)+" joueurs.\n\n", getConsoleWidth()));
rl.question(center('Quel nom donner au fichier?\n', getConsoleWidth()), (answer2) => {
  rl.question(center('Combien de points dommage moyen appliquer aux armes lors de la simulation de partie?\n', getConsoleWidth()), (answer3) => {
    rl.question(center('Combien de parties générer?\n', getConsoleWidth()), (answer4) => {
      rl.question(center('Générer jusqu\'à combien de joueurs ?\n', getConsoleWidth()), (answer5) => {
        rl.question(center('Commencer à simuler '+answer4+' parties pour '+answer5+' joueurs ? (o/n)\n', getConsoleWidth()), (answer) => {
          if(answer == "o" || answer == "O"){
            generer_fichier(answer2+".sql", answer3, answer4, answer5);
          }
          else if(answer == "n" || answer == "N"){
            console.log("OK bye!");
          }
          rl.close();
        });
      })
    });
  });
});
    
