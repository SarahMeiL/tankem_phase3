# -*- coding: utf-8 -*-

from dao_oracle import DaoOracle
import os

import Tkinter
import tkMessageBox
import getpass

Tkinter.Tk().withdraw()

def main():
    dao = DaoOracle()
    while True:
        os.system("cls")
        print("1) cr" + chr(130) + "er un usager")
        print("2) supprimer un usager")
        print("3) quitter")
        choix = int(raw_input("choix: "))

        if choix == 3:
            print("ok bye!")
            break
        elif choix == 1:
            nom = str(raw_input("nom du joueur: ").strip())

            mdp = getpass.getpass("mot de passe: ")
            confirm_mdp = getpass.getpass("confirmer le mot de passe: ")

            while mdp != confirm_mdp:
                print("mot de passe non identique")
                mdp = getpass.getpass("mot de passe: ")
                confirm_mdp = getpass.getpass("confirmer le mot de passe: ")

            msg = dao.ecrire(nom, mdp)
            if msg == "USER_EXISTS_CHOOSE_OTHER_NAME":
                tkMessageBox.showerror(msg, "Le joueur existe déjà. Veuillez choisir un autre nom")
            else:
                tkMessageBox.showinfo("Succès", "Nouveau joueur inséré dans la base de données")
        elif choix == 2:
            nom = str(raw_input("nom du joueur: ").strip())

            msg = dao.delete(nom)
            if msg == "USER_NOT_IN_DB":
                tkMessageBox.showerror(msg, "Le joueur n'existe pas dans la base de données")
            else:
                tkMessageBox.showinfo("Succès", "Joueur supprimé de la base de données")

if __name__ == '__main__':
    main()