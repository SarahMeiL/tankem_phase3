# -*- coding: utf-8 -*-

from dao import Dao
from constants import *

import sys, cx_Oracle
import Tkinter
import tkMessageBox

Tkinter.Tk().withdraw() 

class DaoOracle(Dao):
    def __init__(self):
        try:
            sys.path.append(PATH_ORACLE)    
            dsn_tns = cx_Oracle.makedsn(DB_HOST, 1521, DB_SID)
            chaineConnexion = DB_USERLOGIN + '/' + DB_PASSWORD + '@' + dsn_tns   

            self.connexion = cx_Oracle.connect(chaineConnexion)
            self.curseur = self.connexion.cursor()
        except cx_Oracle.DatabaseError as e:
            tkMessageBox.showerror("Erreur", "Impossible de se connecter à la base de données")
            sys.exit(1)
        

    def ecrire(self, nom, mdp):
        try:
            ret = self.curseur.var(cx_Oracle.NCHAR)
            self.curseur.callproc('creer_joueur_retourne', (nom, mdp, ret))
            return ret.getvalue()
        except cx_Oracle.IntegrityError as e:
            tkMessageBox.showerror("Erreur", "Impossible de créer un usager")
    
    def delete(self, nom):
        ret = self.curseur.var(cx_Oracle.NCHAR)
        self.curseur.callproc('delet_this', (nom, ret))
        # if ret.getvalue() == "USER_NOT_IN_DB":
        #     return False
        # else:
        #     return True
        
        return ret.getvalue()
def main():
    dao = DaoOracle()

if __name__ == '__main__':
    main()
